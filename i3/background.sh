#!/usr/bin/sh

BACKGROUND_DIR=${1:-$HOME/.config/i3/img/}

while true
do
    feh --bg-fill "$BACKGROUND_DIR/$(ls $BACKGROUND_DIR | shuf -n 1)"
    sleep 30s
done
