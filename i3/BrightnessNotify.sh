#!/bin/bash
# Brightnes notification

xbacklight $1 $2

Brightness=$(xbacklight | sed -e 's,^\(.*\)\..*,\1,')

# # base dir for backlight class
# basedir="/sys/class/backlight/"

# # get the backlight handler
# handler=$basedir$(ls $basedir)"/"

# # get current brightness
# old_brightness=$(cat $handler"brightness")

# # get max brightness
# max_brightness=$(cat $handler"max_brightness")

# # get current brightness %
# old_brightness_p=$(( 100 * $old_brightness / $max_brightness ))

# # calculate new brightness % 
# new_brightness_p=$(($old_brightness_p $1 $2))

# # calculate new brightness value
# new_brightness=$(( $max_brightness * $new_brightness_p / 100 ))

# # set the new brightness value
# sudo chmod 666 $handler"brightness"
# # echo $new_brightness > $handler"brightness"

# # Brightness=$new_brightness
# Brightness=$new_brightness_p


# Make the bar with the special character ─ (it's not dash -)
# https://en.wikipedia.org/wiki/Box-drawing_character
bar=$(seq -s "─" $(($Brightness / 5)) | sed 's/[0-9]//g')

if [ "$1" == "get" ]; then
	echo $Brightness
else
	ID=$(cat ~/.config/i3/.dunst_brightnes)
	if [ $ID -gt "0" ];	then
	 	dunstify -p -r $ID " $Brightness% <br> $bar" > ~/.config/i3/.dunst_brightnes
	else
	 	dunstify -p " $Brightness% <br> $bar" > ~/.config/i3/.dunst_brightnes
	fi
fi
