import fontawesome as fa
from i3pystatus import Status

from i3pystatus import playerctl

status = Status(interval=0.5)

# Displays clock like this:
# Tue 30 Jul 11:59:46 PM KW31
#                          ^-- calendar week
status.register("clock", format=fa.icons['clock-o'] + " %H:%M %a %d %b", )

# netifaces

# Shows the address and up/down state of eth0. If it is up the address is shown in
# green (the default value of color_up) and the CIDR-address is shown
# (i.e. 10.10.10.42/24).
# If it's down just the interface name (eth0) will be displayed in red
# (defaults of format_down and color_down)
#
# Note: the network module requires PyPI package netifaces
status.register("network",
                interface="lo",
                format_up="{v4}",
                format_down="",
                )

# Note: requires both netifaces and basiciw (for essid and quality)
status.register("network",
                interface="wlp3s0",
                format_up="%s {quality:02.0f}%% <scroll width_scroll=15><vscroll min_width=15 interval=30>{essid}-{v4}</vscroll></scroll>" % fa.icons['wifi'],
                format_down="%s " % fa.icons['wifi'],
                interval=0.3
                )

status.register("online")

# Shows your CPU temperature, if you have a Intel CPU
status.register("temp", format="{temp:.0f}°C", )

# This would look like this:
# Discharging 6h:51m
status.register("battery",
                format="{status} {percentage_design:.0f}% {remaining:%E%hh:%Mm}",
                alert=True,
                alert_percentage=5,
                status={
                    "DIS": "",
                    "CHR": fa.icons['bolt'],
                    "FULL": "Bat full",
                },
                )

# Shows pulseaudio default sink volume
#
# Note: requires libpulseaudio from PyPI
status.register("pulseaudio", format="♪{volume}", )

# Shows mpd status
# Format:
# Cloud connected▶Reroute to Remain
status.register(playerctl,
                # format="{status} <scroll width_scroll=10>{title}</scroll>",
                format="{status} <scroll width_scroll=20 stop_scroll=8 start_scroll=5>{title}</scroll>",
                format_not_running="",
                status={
                    "paused": "▷",
                    "playing": "▶",
                    "stopped": "◾",
                },
                player_name="MellowPlayer",
                interval=0.5)

status.run()
