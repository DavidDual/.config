from i3pystatus.format.formatter import formatp


class vScrollFormat:
    count = 0
    time = 0
    interval = 0
    min_width = 0

    f = ""

    def main(self, f, **d):
        list = f.split('-')
        if self.time > 0:
            self.time -= 1
        else:
            self.time = self.interval
            self.count += 1
            if self.count >= len(list):
                self.count = 0
        f = formatp(list[self.count], **d)
        if len(f) < self.min_width:
            f = (' ' * (self.min_width - len(f))) + f
        return f

