from i3pystatus.core.util import formatp

from i3pystatus.format.scrollFormat import scrollFormat
from i3pystatus.format.vScrollFormat import vScrollFormat

class decorator:
    def __init__(self, function):
        self.function = function
        self.items = {"scroll": scrollFormat, "vscroll": vScrollFormat}
        self.elcount = 0
        self.elements = [scrollFormat()]

    def html(self, f, **d):
        from html.parser import HTMLParser
        s = self
        s.elcount = 0
        s.elements[s.elcount].f = ""

        class MyHTMLParser(HTMLParser):
            count = 0

            def handle_starttag(self, tag, attrs):
                s.elcount += 1
                if len(s.elements) < s.elcount + 1:
                    s.elements.append(s.items[tag]())
                for i in range(len(attrs)):
                    setattr(s.elements[s.elcount], attrs[i][0], int(attrs[i][1]))
                s.elements[s.elcount].f = ""
                # print("Encountered a start tag:", tag, " ", attrs)

            def handle_endtag(self, tag):
                el = s.elements[s.elcount]
                s.elcount -= 1
                res = el.main(el.f, **d)
                s.elements[s.elcount].f += "{" + tag + str(self.count) + "}"
                d[tag + str(self.count)] = res
                self.count += 1
                # print("Encountered an end tag :", tag)

            def handle_data(self, data):
                if len(s.elements) > 0:
                    s.elements[s.elcount].f += data
                    # print("Encountered some data  :", data)

        parser = MyHTMLParser()
        parser.feed(f)

        return self.elements[0].f, d

    def __call__(self, *args, **kwargs):
        f = args[0]
        f, kwargs = self.html(f, **kwargs)
        return self.function(f, **kwargs)


formatp = decorator(formatp)
