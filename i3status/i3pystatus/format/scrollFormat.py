from i3pystatus.format.formatter import formatp


class scrollFormat:
    start_scroll = 0
    stop_scroll = 0
    width_scroll = 50
    offset = 0

    f = ""

    def scroll_text(self, text):
        if len(text) > self.width_scroll:
            if self.offset < 0:
                text = text[:self.width_scroll]
            elif self.offset + self.width_scroll < len(text):
                text = text[self.offset:self.offset + self.width_scroll]
            else:
                text = text[-self.width_scroll:]
                if self.offset + self.width_scroll >= len(text) + self.stop_scroll:
                    self.offset = -self.start_scroll - 1
            self.offset += 1
        return text

    def main(self, f, **d):
        f = self.scroll_text(formatp(f, **d))
        return f
