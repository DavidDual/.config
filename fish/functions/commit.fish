# Defined in /home/david/.config/fish/config.fish @ line 25
function commit
	git status
	read -P "Commit message:  " -l REPLY
        git commit -m "$REPLY"
	git push
end
