# Defined in /home/david/.config/fish/config.fish @ line 41
function proxy_on --argument address
	export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"

    if [ "$address" != "" ]
        set valid (echo "$address" | sed -n 's/\([0-9]\{1,3\}.\)\{4\}:\([0-9]\+\)/&/p')
        if [ "$valid" != "$address" ]
            echo "Invalid address"
            return 1
        end

        export http_proxy="http://$1/" \
               https_proxy=$http_proxy \
               ftp_proxy=$http_proxy \
               rsync_proxy=$http_proxy
        echo "Proxy environment variable set."
        return 0
    end

    read -P "username: " -l username
    if [ "$username" != "" ]
        read -P "password: " -i -l password
        set -l pre "$username:$password@"
    end

    read -P "server: " -l server
    read -P "port: " -l port
    export http_proxy="http://$pre$server:$port/" \
           https_proxy=$http_proxy \
           ftp_proxy=$http_proxy \
           rsync_proxy=$http_proxy \
           HTTP_PROXY=$http_proxy \
           HTTPS_PROXY=$http_proxy \
           FTP_PROXY=$http_proxy \
           RSYNC_PROXY=$http_proxy
end
