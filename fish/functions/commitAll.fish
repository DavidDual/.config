# Defined in /home/david/.config/fish/config.fish @ line 32
function commitAll
	git status
	read -P "Commit all? [n]" -n 1 -l REPLY
	if echo $REPLY | grep -q -E "^[Yy]\$"
		git add *
		commit
	end
end
