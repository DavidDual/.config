# Defined in /home/david/.config/fish/config.fish @ line 77
function proxy_off
	set -e http_proxy
    set -e https_proxy
    set -e ftp_proxy
    set -e rsync_proxy
    set -e HTTP_PROXY
    set -e HTTPS_PROXY
    set -e FTP_PROXY
    set -e RSYNC_PROXY
    echo -e "Proxy environment variable removed."
end
