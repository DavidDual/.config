# config file for fishshell terminal

alias ls='ls --color=auto'
alias install='sudo pacman -S'
alias update='sudo pacman -Syu'
alias yinstall='yaourt -S'
alias ylist='yaourt -Syu --aur'
alias yupdate='yaourt -Syu --aur --noconfirm'
alias remove='sudo pacman -Rns'
alias ll='ls -la'
alias img='feh -q -. -d'
alias imgf='img -F'
alias sshSubl3='ssh -R 52698:localhost:52698'

alias cacheclear='echo "echo 1 > /proc/sys/vm/drop_caches" | sudo sh & echo "echo 2 > /proc/sys/vm/drop_caches" | sudo sh & echo "echo 3 > /proc/sys/vm/drop_caches" | sudo sh'
alias reload='source ~/.config/fish/config.fish'


if [ command -v subl >/dev/null 2>&1 ]
else
alias subl='subl3'
end


function commit
	git status
	read -P "Commit message:  " -l REPLY
        git commit -m "$REPLY"
	git push
end

function commitAll
	git status
	read -P "Commit all? [n]" -n 1 -l REPLY
	if echo $REPLY | grep -q -E "^[Yy]\$"
		git add *
		commit
	end
end

function proxy_on --argument address
    export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"

    if [ "$address" != "" ]
        set valid (echo "$address" | sed -n 's/\([0-9]\{1,3\}.\)\{4\}:\([0-9]\+\)/&/p')
        if [ "$valid" != "$address" ]
            echo "Invalid address"
            return 1
        end

        export http_proxy="http://$1/" \
               https_proxy=$http_proxy \
               ftp_proxy=$http_proxy \
               rsync_proxy=$http_proxy
        echo "Proxy environment variable set."
        return 0
    end

    read -P "username: " -l username
    if [ "$username" != "" ]
        read -P "password: " -i -l password
        set -l pre "$username:$password@"
    end

    read -P "server: " -l server
    read -P "port: " -l port
    export http_proxy="http://$pre$server:$port/" \
           https_proxy=$http_proxy \
           ftp_proxy=$http_proxy \
           rsync_proxy=$http_proxy \
           HTTP_PROXY=$http_proxy \
           HTTPS_PROXY=$http_proxy \
           FTP_PROXY=$http_proxy \
           RSYNC_PROXY=$http_proxy
end

function proxy_off
    set -e http_proxy
    set -e https_proxy
    set -e ftp_proxy
    set -e rsync_proxy
    set -e HTTP_PROXY
    set -e HTTPS_PROXY
    set -e FTP_PROXY
    set -e RSYNC_PROXY
    echo -e "Proxy environment variable removed."
end


funcsave commit
funcsave commitAll
funcsave proxy_on
funcsave proxy_off
